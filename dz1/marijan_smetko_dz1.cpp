#include <iostream>
#include <fstream>
#include <assert.h>
#include <array>
#include <cmath>
#include <iomanip>

const int BLOCK_HEIGHT = 8;
const int BLOCK_WIDTH = 8;
const int COLOR_CHANNELS = 3;

double K1[8][8] = {
    {16, 11, 10, 16,  24,  40,  51,  61},
    {12, 12, 14, 19,  26,  58,  60,  55},
    {14, 13, 16, 24,  40,  57,  69,  56},
    {14, 17, 22, 29,  51,  87,  80,  62},
    {18, 22, 37, 56,  68, 109, 103,  77},
    {24, 35, 55, 64,  81, 104, 113,  92},
    {49, 64, 78, 87, 103, 121, 120, 101},
    {72, 92, 95, 98, 112, 100, 103,  99}
};
double K2[8][8] = {
    {17, 18, 24, 47, 99, 99, 99, 99},
    {18, 21, 26, 66, 99, 99, 99, 99},
    {24, 26, 56, 99, 99, 99, 99, 99},
    {47, 66, 99, 99, 99, 99, 99, 99},
    {99, 99, 99, 99, 99, 99, 99, 99},
    {99, 99, 99, 99, 99, 99, 99, 99},
    {99, 99, 99, 99, 99, 99, 99, 99},
    {99, 99, 99, 99, 99, 99, 99, 99},
};

typedef struct {
    std::string type;
    int width;
    int height;
    int maxval;
} PPMMetadata;

PPMMetadata get_metadata(std::ifstream &image) {
    PPMMetadata metadata;
    image >> metadata.type >> metadata.width >> metadata.height >> metadata.maxval;
    // std::cout << metadata.type << " (" << metadata.height << ", " << metadata.width << ") " << metadata.maxval << std::endl;
    return metadata;
}

void DCT(const double (&matrix)[BLOCK_HEIGHT][BLOCK_WIDTH], double (&output)[BLOCK_HEIGHT][BLOCK_WIDTH]) {
    for (int u = 0; u < BLOCK_HEIGHT; u++) {
        for (int v = 0; v < BLOCK_WIDTH; v++) {
            double c1 = (u == 0 ? 1 : M_SQRT2);
            double c2 = (v == 0 ? 1 : M_SQRT2);
            double c = c1*c2 / std::sqrt(BLOCK_HEIGHT*BLOCK_WIDTH);
            for (int i = 0; i < BLOCK_HEIGHT; i++) {
                for (int j = 0; j < BLOCK_WIDTH; j++) {
                    double val = matrix[i][j];
                    val *= std::cos((2*i+1)*u*M_PI/(2*BLOCK_HEIGHT));
                    val *= std::cos((2*j+1)*v*M_PI/(2*BLOCK_WIDTH));
                    output[u][v] += val;
                }
            }
            output[u][v] *= c;
        }
    }
}

void get_rgb_block(std::ifstream &image, int blocknum, int height, int width, uint8_t (&block)[BLOCK_HEIGHT][BLOCK_WIDTH][COLOR_CHANNELS]) {
    int blocks_per_width = width / BLOCK_WIDTH;
    int block_row = blocknum / blocks_per_width;
    int block_col = blocknum % blocks_per_width;
    // std::cout << block_row << " " << block_col << std::endl;

    // skipping the first block_row block rows to get to the row where our block is
    // also skip one whitespace
    image.ignore(1);
    image.ignore(block_row * BLOCK_HEIGHT * width * COLOR_CHANNELS);

    for (int i = 0; i < BLOCK_HEIGHT; i++) {
        // skipping the first block_col bytes
        // std::cout << COLOR_CHANNELS * block_col * BLOCK_WIDTH << std::endl;
        image.ignore(COLOR_CHANNELS * block_col * BLOCK_WIDTH);
        
        // read BLOCK_WIDTH pixels
        for (int j = 0; j < BLOCK_WIDTH; j++) {
            // for every pixel, read COLOR_CHANNELS colors
            for (int c = 0; c < COLOR_CHANNELS; c++) {
                image >> block[i][j][c];
            }
        }

        // fast forward to the end of the row
        image.ignore((width - (block_col+1)*BLOCK_WIDTH)*COLOR_CHANNELS);
    }
    
    return;
}

void process(uint8_t (&block)[BLOCK_HEIGHT][BLOCK_WIDTH][COLOR_CHANNELS],
    const std::array<double, 3> &coefs,
    const double &intr, const double (&q_table)[BLOCK_HEIGHT][BLOCK_WIDTH],
    std::ofstream &output) {
    double values[BLOCK_HEIGHT][BLOCK_WIDTH] = {0.0};
    double dct[BLOCK_HEIGHT][BLOCK_WIDTH] = {0.0};
    for (int i = 0; i < BLOCK_HEIGHT; i++) {
        for (int j = 0; j < BLOCK_WIDTH; j++) {
            for (int k = 0; k < COLOR_CHANNELS; k++) {
                values[i][j] += ((double)block[i][j][k]) * coefs[k];
            }
            values[i][j] += intr - 128;
        }
    }
    DCT(values, dct);
    for (int i = 0; i < BLOCK_HEIGHT; i++) {
        for (int j = 0; j < BLOCK_WIDTH; j++) {
            int val = std::round(dct[i][j] / q_table[i][j]);
            if (i < BLOCK_WIDTH-1) output << val << '\t';
        }
        output << std::endl;
    }
    
    return;
}

int main(int argc, char* argv[]) {
    assert(argc == 3+1);
    std::string filename = argv[1];
    int block_number = std::stoi(std::string(argv[2]));
    std::string out_filename = std::string(argv[3]);

    std::ifstream image (filename);
    if (!image.is_open()) return 1;
    PPMMetadata metadata = get_metadata(image);
    uint8_t rgb_block[BLOCK_WIDTH][BLOCK_HEIGHT][COLOR_CHANNELS];
    get_rgb_block(image, block_number, metadata.height, metadata.width, rgb_block);
    image.close();
    std::ofstream out_file (out_filename);

    std::array<double, 3> Y_coef = {0.299, 0.587, 0.114};
    double Y_intr = 0.0;
    process(rgb_block, Y_coef, Y_intr, K1, out_file);
    out_file << std::endl;

    std::array<double, 3> Cb_coef = {-0.1687, -0.3313, 0.5};
    double Cb_intr = 128;
    process(rgb_block, Cb_coef, Cb_intr, K2, out_file);
    out_file << std::endl;

    std::array<double, 3> Cr_coef = {0.5, -0.4187, -0.0813};
    double Cr_intr = 128;
    process(rgb_block, Cr_coef, Cr_intr, K2, out_file);
    out_file << std::flush;
    out_file.close();
    return 0;
}