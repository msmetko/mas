#include <math.h>
#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>

#define MIN(a, b) (((a) < (b)) ? (a) : (b))
#define MAX(a, b) (((a) > (b)) ? (a) : (b))

typedef struct {
    int width;
    int height;
    int max;
    uint8_t *image;
} Image;

Image read_metadata(FILE *image_file) {
    Image image;
    char p5[3];
    fscanf(image_file, "%s\n", p5);
    fscanf(image_file, "%d\n", &image.width);
    fscanf(image_file, "%d\n", &image.height);
    fscanf(image_file, "%d\n", &image.max);
    return image;
}

Image get_image(char *filename) {
    FILE *file = fopen(filename, "r");
    if (file == NULL) {
        printf("Error opening file\n");
        exit(1);
    }
    Image image = read_metadata(file);
    image.image = calloc(image.height*image.width, sizeof(uint8_t));
    if (image.image == NULL) {
        printf("Error loading image: not enough memory");
        exit(1);
    }
    fread(image.image, sizeof(uint8_t), image.width*image.height, file);
    fclose(file);
    return image;
}

uint8_t get_pixel_at(Image *image, int row, int column) {
    return image->image[row * image->width + column];
}

void get_move_vector(Image *image_prev, Image *image_next, int block) {
    int block_top = (block / 32) * 16;
    int block_left = (block % 32) * 16;
    int min_top = MAX(0, block_top-16);
    int max_top = MIN(block_top+16, image_prev->height - 16);
    int min_left = MAX(0, block_left-16);
    int max_left = MIN(block_left+16, image_prev->width - 16);
    int min_mad = 0x7FFFFFFF;
    int min_u=0, min_v=1;
    // x-axis: left, u, j
    // y-axis: top, v, i
    for (int v = min_top; v <= max_top; v++) {
        for (int u = min_left; u <= max_left; u++) {
            // (u, v) are the coordinates of a top-left corner of a new block
            int MAD = 0;
            for (int i = 0; i < 16; i++) {
                for (int j = 0; j < 16; j++) {
                    int prev_pixel = (int) get_pixel_at(image_prev, v+i, u+j);
                    int next_pixel = (int) get_pixel_at(image_next, block_top + i, block_left+j);
                    MAD += abs(prev_pixel - next_pixel);
                }
            }
            if (MAD < min_mad) {
                min_mad = MAD;
                min_u = u;
                min_v = v;
            }
        }
    }
    // printf("%d,%d\n", min_u - block_left, min_v - block_top);
    printf("(%d,%d), MAD: %lf\n", min_u - block_left, min_v - block_top, ((double) min_mad) / (16*16));
}

int main(int argc, char* argv[]) {
    if (argc < 2) {
        printf("Provide block as argument\n");
        exit(1);
    }
    int block = atoi(argv[1]);
    Image image = get_image("lenna.pgm");
    Image image1 = get_image("lenna1.pgm");
    get_move_vector(&image, &image1, block);
    free(image1.image);
    return 0;
}
