#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>

typedef struct {
    int width;
    int height;
    int max;
} Metadata;

Metadata read_metadata(FILE *image) {
    Metadata m;
    char p5[3];
    fscanf(image, "%s\n", p5);
    fscanf(image, "%d\n", &m.width);
    fscanf(image, "%d\n", &m.height);
    fscanf(image, "%d\n", &m.max);
    return m;
}


void process(FILE *image, Metadata *m) {
    int histogram[16] = {0};
    int MASK = 0b11110000;
    for (int i = 0; i < m->height; i++) {
        for (int j = 0; j < m->width; j++) {
            uint8_t val;
            fscanf(image, "%c", &val);
            int index = (val & MASK) >> 4;
            histogram[index]++;
        }
    }
    for (int i = 0; i < 16; i++) {
        printf("%d %f\n", i, ((float)histogram[i])/(m->height * m->width));
    }
}

int main(int argc, char* argv[]) {
    if (argc < 2) {
        printf("Provide input .pgm image as argument\n");
        exit(1);
    }
    //printf("%s\n", argv[1]);
    FILE *file = fopen(argv[1], "r");
    if (file == NULL) {
        printf("Error opening file\n");
        exit(1);
    }
    Metadata metadata = read_metadata(file);
    // printf("\t%d %d %d", metadata.width, metadata.height, metadata.max);
    process(file, &metadata);
    fclose(file);
    return 0;
}
